@extends('layouts.main')
@section('title') Cliente @endsection
@section('content')
    <div class="">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-home"></i> Home </a></li>
            <li class="active">
                Cliente
            </li>
        </ol>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Cliente</h2>
                        <a href="{{url('admin/clientes/create')}}" class="btn btn-success pull-right">
                            Novo Cliente
                        </a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @include('parts.messages')
                        <table class="table data_table">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>E-mail</th>
                                <th>CPF</th>
                                <th>Rua</th>
                                <th>Número</th>
                                <th>Logradouro</th>
                                <th>UF</th>
                                <th>Bairro</th>
                                <th>Opções</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($clients as $c)
                                <tr>
                                    <td style="vertical-align: middle;">{{$c->nome}}</td>
                                    <td style="vertical-align: middle;">{{$c->email}}</td>
                                    <td style="vertical-align: middle;">{{$c->cpf}}</td>
                                    <td style="vertical-align: middle;">{{$c->rua}}</td>
                                    <td style="vertical-align: middle;">{{$c->numero}}</td>
                                    <td style="vertical-align: middle;">{{$c->logradouro}}</td>
                                    <td style="vertical-align: middle;">{{$c->uf}}</td>
                                    <td style="vertical-align: middle;">{{$c->bairro}}</td>
                                    <td>
                                        <a class="btn btn-primary" href="{{url('admin/clientes/'.$c->id.'/edit')}}">
                                            <i class="fa fa-edit"></i> Editar
                                        </a>
                                        <a class="btn btn-danger" href="#" data-toggle="modal" data-target="#delete{{$c->id}}" data-id="{{$c->id}}">
                                            <i class="fa fa-times"></i> Excluir
                                        </a>
                                        @include('components.modal_delete', [ 'url' => 'admin/clientes/'.$c->id, 'id' => $c->id])
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
