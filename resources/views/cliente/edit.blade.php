@extends('layouts.main')
@section('title') Clientes @endsection
@section('content')
    <div class="">
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-home"></i> Home </a></li>
            <li><a href="{{url('admin/clientes')}}"><i class="fa fa-users"></i> Clientes </a></li>
            <li class="active">
                Editar
            </li>
        </ol>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Clientes</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @include('parts.messages')
                        <form class="row" action="{{url('admin/clientes/'.$cliente->id)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input name="_method" type="hidden" value="PUT">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nome">Nome</label>
                                    <input name="nome" class="form-control" type="text" id="nome" required value="{{$cliente->nome}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">E-mail</label>
                                    <input name="email" class="form-control" type="email" id="email" required value="{{$cliente->email}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Telefone</label>
                                    <input name="telefone" class="form-control" type="input" id="telefone" value="{{$cliente->telefone}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cpf">CPF</label>
                                    <input name="cpf" class="form-control" type="input" id="cpf" value="{{$cliente->cpf}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="logradouro">Logradouro</label>
                                    <input name="logradouro" class="form-control" type="input" id="logradouro" value="{{$cliente->logradouro}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="senha">Número</label>
                                    <input name="numero" class="form-control" type="input" id="numero" value="{{$cliente->numero}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="senha">rua</label>
                                    <input name="rua" class="form-control" type="input" id="rua" value="{{$cliente->rua}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="senha">Cidade</label>
                                    <input name="cidade" class="form-control" type="input" id="cidade" value="{{$cliente->cidade}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="senha">Bairro</label>
                                    <input name="bairro" class="form-control" type="input" id="bairro" value="{{$cliente->bairro}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="senha">UF</label>
                                    <input name="uf" class="form-control" type="input" id="uf" value="{{$cliente->uf}}" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <a href="{{url('usuarios')}}" class="btn btn-info">
                                    Voltar
                                </a>
                                <input type="hidden" value="{{$cliente->id}}" name="id">
                                <button id="btnEditar" type="submit" class="btn btn-success">Editar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
