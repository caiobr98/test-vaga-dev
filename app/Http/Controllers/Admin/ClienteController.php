<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Clients\ClientRepository;

class ClienteController extends Controller
{
    private $client;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->client = $clientRepository;
    }

    public function index()
    {
        $client = $this->client->index();

        return view('cliente.index')->with([
            'clients' => $this->client->index()
        ]);
    }
    
    public function create()
    {
        return view('cliente.create');
    }

    public function store(Request $request)
    {
        $client = $this->client->store($request);

        return $client;
    }
    
    public function edit($id)
    {
        $client = $this->client->edit($id);

        return view('cliente.edit')->with([
            'cliente' => $client
        ]);
    }

    public function update(Request $request, $id)
    {
        return $client = $this->client->update($request->all());
    }
    
    public function destroy($id)
    {
        return $client = $this->client->destroy($id);
    }
}
