<?php

namespace App\Repositories\Clients;

use App\Models\Endereco\Endereco;
use App\Models\Cliente\Cliente;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ClientRepository
{   
    public function index()
    {
        $client = Cliente::selectRaw(
                "
                    clients.id,
                    clients.nome,
                    clients.email,
                    clients.cpf,
                    clients.telefone,
                    enderecos.rua,
                    enderecos.logradouro,
                    enderecos.numero,
                    enderecos.bairro,
                    enderecos.uf,
                    enderecos.cidade
                "
            )
            ->join('enderecos', 'enderecos.id_cliente', 'clients.id')
            ->get();
        return $client;
    }
    
    public function store($data)
    {
        try {
            DB::beginTransaction();
            $client = Cliente::create([
                'nome' => $data->nome,
                'email' => $data->email,
                'cpf' => $data->cpf,
                'telefone' => $data->telefone
            ]);
    
            $endereco = Endereco::create([
                'id_cliente' => $client->id,
                'numero'=> $data->numero,
                'logradouro'=> $data->logradouro,
                'rua'=> $data->rua,
                'bairro'=> $data->bairro,
                'cidade'=> $data->cidade,
                'uf'=> $data->uf
            ]);

            DB::commit();

            return redirect('admin/clientes')->with('success', 'O cliente foi cadastrado com sucesso');
        } catch (\Throwable $th) {
            DB::rollBack();
            return back('admin/clientes')->with('error', 'Erro ao cadastrar cliente');
            //throw $th;
        }
    }

    public function edit($id) {
        return Cliente::selectRaw(
            "
                clients.id,
                clients.nome,
                clients.email,
                clients.cpf,
                clients.telefone,
                enderecos.rua,
                enderecos.logradouro,
                enderecos.numero,
                enderecos.bairro,
                enderecos.uf,
                enderecos.cidade
            "
        )
        ->join('enderecos', 'enderecos.id_cliente', 'clients.id')
        ->where('clients.id',$id)
        ->first();
    }

    public function update($data){
        try {
            DB::beginTransaction();

            $client = Cliente::findOrFail($data['id']);

            $client->update([
                'nome' => $data['nome'],
                'email' => $data['email'],
                'cpf' => $data['cpf'],
                'telefone' => $data['telefone']
            ]);

            Endereco::where('id_cliente', $data['id'])
            ->update([
                'numero'=> $data['numero'],
                'logradouro'=> $data['logradouro'],
                'rua'=> $data['rua'],
                'bairro'=> $data['bairro'],
                'cidade'=> $data['cidade'],
                'uf'=> $data['uf']
            ]);

            DB::commit();

            return redirect('admin/clientes')->with('success', 'O cliente foi editado com sucesso');
        } catch (\Throwable $th) {
            DB::rollBack();
            return back('admin/clientes')->with('error', 'Erro ao editar cliente');
        }
    }

    public function destroy($id){
        try {
            DB::beginTransaction();

            $client = Cliente::findOrFail($id);

            $client->delete();

            dd($client);
            DB::commit();

            return redirect('admin/clientes')->with('success', 'O cliente foi apagado com sucesso');
        } catch (\Throwable $th) {
            DB::rollBack();
            return back('admin/clientes')->with('error', 'Erro ao apagar cliente');
        }
    }

}