<?php

namespace App\Models\Cliente;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends BaseModel
{
    protected $table = 'clients';
    use SoftDeletes;
}
