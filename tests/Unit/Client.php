<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Cliente\Cliente;

class Client extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testDeleteClient()
    {
        $fake_id = 4;
        $client = Cliente::findOrFail($fake_id);

        $client->delete();

        $this->assertEmpty($client);
    }
}
